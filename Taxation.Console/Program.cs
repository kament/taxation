﻿using Taxation.Business;
using Taxation.Business.Rules;

namespace Taxation.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Enter salary in IDR");

            var salary = decimal.Parse(System.Console.ReadLine());

            var engine = new TaxationEngine(new ITaxationRule[]
            {
                new MinimumTaxationRule(1000, 10),
                new SocialContributionRule(1000, 3000, 15)
            });

            var netSalary = engine.CalculateNetAmount(salary);

            System.Console.WriteLine($"Your net salary is {netSalary} IDR");
        }
    }
}
