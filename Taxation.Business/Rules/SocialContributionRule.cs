﻿namespace Taxation.Business.Rules
{
    public class SocialContributionRule : ITaxationRule
    {
        private readonly decimal minTaxableAmount;
        private readonly decimal maxTaxableAmount;
        private readonly decimal taxInPercent;

        public SocialContributionRule(decimal minTaxableAmount, decimal maxTaxableAmount, decimal taxInPercent)
        {
            this.minTaxableAmount = minTaxableAmount;
            this.maxTaxableAmount = maxTaxableAmount;
            this.taxInPercent = taxInPercent;
        }

        public Tax CalculateTax(decimal salary)
        {
            if (salary > minTaxableAmount)
            {
                var taxableAmount = GetTaxableAmount(salary);

                return new Tax(taxInPercent, taxableAmount);
            }
            else
            {
                return new NoTax();
            }
        }

        private decimal GetTaxableAmount(decimal salary)
        {
            var taxableAmount = salary;

            if (taxableAmount > maxTaxableAmount)
            {
                taxableAmount = maxTaxableAmount;
            }

            return taxableAmount - minTaxableAmount;
        }
    }
}
