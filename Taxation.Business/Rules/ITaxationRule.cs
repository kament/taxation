﻿namespace Taxation.Business.Rules
{
    public interface ITaxationRule
    {
        Tax CalculateTax(decimal salary);
    }
}
