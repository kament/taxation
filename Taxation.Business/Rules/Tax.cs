﻿namespace Taxation.Business.Rules
{
    public class Tax
    {
        public Tax(decimal taxationPercent, decimal taxableAmount)
        {
            TaxationPercent = taxationPercent;
            TaxableAmount = taxableAmount;
        }

        public decimal TaxationPercent { get; }

        public decimal TaxableAmount { get; }
    }

    internal class NoTax : Tax
    {
        public NoTax()
            : base(0M, 0M)
        {
        }
    }
}
