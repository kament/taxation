﻿using Taxation.Business.Rules;

namespace Taxation.Business.Rules
{
    public class MinimumTaxationRule : ITaxationRule
    {
        private readonly decimal minTaxableAmount;
        private readonly decimal taxationInPercent;

        public MinimumTaxationRule(decimal minTaxableAmount, decimal taxationInPercent)
        {
            this.minTaxableAmount = minTaxableAmount;
            this.taxationInPercent = taxationInPercent;
        }

        public Tax CalculateTax(decimal salary)
        {
            if (salary > minTaxableAmount)
            {
                var taxableAmount = GetTaxableAmount(salary);

                return new Tax(taxationInPercent, taxableAmount);
            }
            else
            {
                return new NoTax();
            }
        }

        private decimal GetTaxableAmount(decimal salary)
        {
            return salary - minTaxableAmount;
        }
    }
}
