﻿using System.Collections.Generic;
using System.Linq;
using Taxation.Business.Rules;

namespace Taxation.Business
{
    public class TaxationEngine
    {
        private readonly IEnumerable<ITaxationRule> taxationRules;

        public TaxationEngine(IEnumerable<ITaxationRule> taxationRules)
        {
            this.taxationRules = taxationRules;
        }

        public decimal CalculateNetAmount(decimal amount)
        {
            var taxes = taxationRules.Select(r => r.CalculateTax(amount)).Sum(t => t.TaxationPercent * t.TaxableAmount / 100);

            return amount - taxes;
        }
    }
}
