﻿using NUnit.Framework;
using Taxation.Business.Rules;

namespace Taxation.Business.Tests
{
    public class TaxationEngineTests
    {
        private TaxationEngine engine;

        [SetUp]
        public void Setup()
        {
            engine = new TaxationEngine(new ITaxationRule[]
            {
                new MinimumTaxationRule(1000, 10),
                new SocialContributionRule(1000, 3000, 15)
            });
        }

        [Test]
        [TestCase(980, 980)]
        [TestCase(3400, 2860)]
        [TestCase(4000, 3400)]
        public void Calculate_Tax_Corectly(decimal amountToTax, decimal expectedNetAmount)
        {
            var netAmount = engine.CalculateNetAmount(amountToTax);

            Assert.That(netAmount, Is.EqualTo(expectedNetAmount));
        }
    }
}
