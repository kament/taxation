using NUnit.Framework;
using Taxation.Business.Rules;

namespace Tests
{
    public class Tests
    {
        private MinimumTaxationRule rule;

        [SetUp]
        public void Setup()
        {
            rule = new MinimumTaxationRule(1000, 10);
        }

        [Test]
        [TestCase(999, 0)]
        [TestCase(1000, 0)]
        [TestCase(2000, 10)]
        public void Should_Return_Correct_Tax_Percent(decimal amountToTax, decimal expectedTaxPercents)
        {
            var tax = rule.CalculateTax(amountToTax);

            Assert.That(tax.TaxationPercent, Is.EqualTo(expectedTaxPercents));
        }

        [Test]
        [TestCase(999, 0)]
        [TestCase(1000, 0)]
        [TestCase(2000, 1000)]
        [TestCase(3000, 2000)]
        public void Should_Return_Correct_Taxable_Amount(decimal amountToTax, decimal taxableAmount)
        {
            var tax = rule.CalculateTax(amountToTax);

            Assert.That(tax.TaxableAmount, Is.EqualTo(taxableAmount));
        }
    }
}